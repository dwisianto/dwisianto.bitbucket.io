



##
##
## Convert all README.md to README.md.html
##
README_MD=$(shell find . -iname "README.md") # Find all markdown files
README_HTML=$(README_MD:.md=.html) # Form all 'html' counterparts

##
.PHONY = all 
all: $(README_HTML)

readme_touch:
	@echo $(README_MD)
	@echo $(README_HTML)
	touch $(README_MD)
	touch $(README_HTML)
	
readme_clean:
	rm $(README_HTML)
	
%.html: %.md
	pandoc --from markdown --to html $< -o $@
	
