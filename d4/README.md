
## Credential


| [Writing](wt) |   |
| ---- | ----- |
| [pandoc]() | |
| [markdown]() | |
| [equation]() | |
| [tableOfContent]() | |
| [jabref](https://iforgot.apple.com) |    |


# Location

* [resume_bib](d18/resume/d18.bib.html)
* [attitude](d18/attitude)
* [d18](d18)
* [pandoc_resume](http://mszep.github.io/pandoc_resume/)
* [d18_resume_r18a_pandoc_resume](d18/resume/r18a/pandoc_resume/)

* Patent Examiner [uspto](https://www.uspto.gov/)
