# dwisianto

This site contains the following pages:

1. Focus
    1. [do_bb_io](../dwisianto.bitbucket.io) - [do_bb_a18](../dwisianto.bitbucket.a18/README.html) - [do_gh_io](../dwisianto.github.io/) =[d_whcs](../d.bee.whcs)
        1. Organize
        2. [k2](k2){target="_blank"} - [kCld](k2/kCld) - [kMac](k2/kMac) - [kPy](k2/kXX/kPy/README.html) = [kJv](k2/kXX/kJv)
        3. [w3](w3){target="_blank"} - [y18](w3/y18) - [y18-bib](w3/y18/y18.bib){target="_blank"} [y18bib-html](w3/y18/y18.bib.html)
            1. [summa](w3/y18/summ/README.html) - Structured prediction - [trie]()  
            3. NDCG - Bias Variance Margin Decomposition
        4. [d4](d4){target="_blank"} - [Resume](d4/d18/resume/r18a/README.md) - [bib](d4/d18/resume/d18.bib.html)
        5. [v5] - visualization
            1. [brat Annotation Tools ](http://brat.nlplab.org/sitemap.html) - [Entities](../../../w3/l17/brat/v14ex/b14aEntities.html) - [Attributes](../../../w3/l17/brat/v14ex/b14bAttribute.html) - [Relations](../../../w3/l17/brat/v14ex/b14cRelation.html) - [Events](../../../w3/l17/brat/v14ex/b14dEvent.html)
            2. [Markdown Presentation](../../../w3/yXX/md/README.html)
            3. [Annotation Visualization](../../../w3/yXX/anno/anno.html)
            4. [Markdown Writing](../../../w3/yXX/wt/w1a/)
2. [Koding](k2)
    1. [gcp](k2/kCls/README.html) - [console](https://console.cloud.google.com/)   [gcloud](https://cloud.google.com/)  
    2. [nas](https://find.synology.com) - 192.168.1.227
    3. [osx](k2/kMac/README.html) - [appleid](https://appleid.apple.com)  [iforgot](https://iforgot.apple.com) [icloud](https://www.icloud.com/)
    4. [dbee](/Users/dsm) - [DSc](/Users/dsm/DSc)    - [ssh](/Users/dsm/DSc/.ssh)
    5. [dwyk](/Users/dwyk) - [Desktop](/Users/dwyk/Desktop) - [Download](/Users/dwyk/Download) - [Document](/Users/dwyk/Document) - [bash_prof](/Users/dwyk/.bash_profile)
        1. [dwyk](/Users/dwyk) - [ssh](/Users/dwyk/.ssh) - [caskroom](/Usr/local/Caskroom) - [maven](~/.m2)
        2. [d7](/Users/dwyk/d7)
        3. [ws](/Users/dwyk/d7/ws)            
        4. [db](/Users/dwyk/d7/db) - [deepmind](/Users/dwyk/d7/db/rc/deepmind/)
        5. [gb](/Users/dwyk/d7/gb)            
        6. [gt](/Users/dwyk/d7/gb)
3. Dataset
    1. This dataset concentrates on four types of named entities: persons, locations, organizations and names of miscellaneous entities that do not belong to the previous three groups. It provides standard training/development/test splits.
    2. http://josephsmarr.com/2007/01/27/my-most-famous-nlp-paper-conll-03/
    3. https://www.clips.uantwerpen.be/conll2003/ner/
    4. https://github.com/CogComp/cogcomp-nlp/tree/master/ner
    5. https://github.com/miso-belica/sumy
9. Misc
    1. [bio](https://en.wikipedia.org/wiki/Bioinformatics)
    2. [maa](https://www.maa.org)
    3. [norvig_spell_correct](https://norvig.com/spell-correct.html)


## Candia
### Cars
#### Lawn

## Journal

1. Journey
2. brew cask install maven
3. wood conditioner, stain, oil Polyurethane
216.1  brew install gs > compressing pdf for storage
301. Travel and BDay
501. Driver License

http://www.diynetwork.com/how-to/skills-and-know-how/painting/how-to-apply-stain-varnish-wax-dye-or-oil-to-wood
Follow these simple steps to apply stain, varnish, wax, dye or oil to enhance the look of wood.
