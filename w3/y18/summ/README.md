
# Summarization


Reading comprehension (RC) is the ability to read text, process it, and understand its meaning.
[Wikipedia]()

|    |    |
|----|----|
|    |    |

## CNN-DailyMail

 [pdf](https://arxiv.org/abs/1506.03340) [local](../../../../dwisianto.bitbucket.a18/w3/y18/summ/rd/hermann2015teaching.pdf) [code](https://github.com/deepmind/rc-data)

```
@inproceedings{hermann2015teaching,
  title={Teaching machines to read and comprehend},
  author={Hermann, Karl Moritz and Kocisky, Tomas and Grefenstette, Edward and Espeholt, Lasse and Kay, Will and Suleyman, Mustafa and Blunsom, Phil},
  booktitle={Advances in Neural Information Processing Systems},
  pages={1693--1701},
  year={2015}
}
```

Progress on the path from shallow bag-of-words information retrieval algorithms to machines capable of reading and understanding documents has been slow.  
Traditional approaches to machine reading and comprehension have been based on either hand engineered grammars [1],
or information extraction methods of detecting predicate argument triples that can later be queried as a relational database [2].
Supervised machine learning approaches have largely been absent from this space due to both the lack of large scale training datasets,
and the difficulty in structuring statistical models flexible enough to learn to exploit document structure.


While obtaining supervised natural language reading comprehension data has proved difficult,
some researchers have explored generating synthetic narratives and queries [3, 4].
Such approaches allow the generation of almost unlimited amounts of supervised data
and enable researchers to isolate the performance of their algorithms on individual simulated phenomena.
Work on such data has shown that neural network based models hold promise for modeling reading comprehension,
something that we will build upon here.   
Historically,  however,  
many similar approaches in Computational Linguistics have failed to manage the transition from synthetic data to real environments,
as such closed worlds inevitably fail to capture the complexity, richness, and noise of natural language [5].


## CNN-Daily Mail Thorough

[pdf](https://arxiv.org/pdf/1606.02858.pdf)
[local](../../../../dwisianto.bitbucket.a18/w3/y18/summ/rd/chen2016through.pdf)
[code]()

```bash
@inproceedings{chen2016thorough,
    title={A Thorough Examination of the {CNN/Daily Mail} Reading Comprehension Task},
    author={Chen, Danqi and Bolton, Jason and Manning, Christopher D.},
    booktitle={Association for Computational Linguistics (ACL)},
    year={2016},
    url={https://arxiv.org/pdf/1606.02858.pdf}
  }
```


Recently, researchers at DeepMind (Hermann et al.,  2015) had the appealing,  
original idea of exploiting the fact that
the abundant news articles of CNN and Daily Mail are accompanied by bullet point summaries
in order to heuristically create large-scale supervised training data for the reading comprehension task.
 Figure 1 gives an example.
Their idea is that a bullet point usually summarizes one or several aspects of the article.
If the computer understands the content of the article,
it should be able to infer the missing entity in the bullet point.


## Sentence Selection

[Jianpeng Cheng](http://homepages.inf.ed.ac.uk/s1537177/)
[pdf](https://arxiv.org/pdf/1603.07252.pdf)
[lcl](../../../../dwisianto.bitbucket.a18/w3/y18/summ/rd/)
[code](https://github.com/danqi/rc-cnn-dailymail)
[lcl]



## Nallapati



## TimeLine


## RACE

https://cs.stanford.edu/~danqi/
[RACE](https://github.com/qizhex/RACE_AR_baselines)


## Cloze

```
@article{taylor1953cloze,
  title={“Cloze procedure”: A new tool for measuring readability},
  author={Taylor, Wilson L},
  journal={Journalism Bulletin},
  volume={30},
  number={4},
  pages={415--433},
  year={1953},
  publisher={SAGE Publications Sage CA: Los Angeles, CA}
}
```
