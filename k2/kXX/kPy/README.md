

## Python

##
* [virtualenv](https://www.dabapps.com/blog/introduction-to-pip-and-virtualenv-python/)
* [package](http://python-packaging.readthedocs.io/en/latest/minimal.html)

## Package vs Module



## Language

### Python

| column | column | column |
|--------|--------|--------|
| type | |
| isinstance | |
| locals | |
| globals | |
| dir | |


### pip


Python actually has a more primitive package manager called easy_install,
which is installed automatically when you install Python itself.
pip is vastly superior to easy_install for lots of reasons,
and so should generally be used instead.
You can use easy_install to install pip as follows:

```
#
sudo easy_install pip
#
sudo pip install virtualenv
sudo pip install --upgrade virtualenv
```

### virtualenv

If you already have pip, the easiest way is to install it globally sudo pip install virtualenv.
Usually pip and virtualenv are the only two packages you ever need to install globally,
because once you have got both of these you can do all your work inside virtual environments.

In fact, virtualenv comes with a copy of pip which gets copied into every new environment you create,
so virtualenv is really all you need.
You can even install it as a separate standalone package (rather than from PyPI).

```
cd ~/myProject/run/env
virtualenv e1a
# add e1a to .gitignore
#
which python
source e1a/bin/activate
which python
env/bin/pip install -r requirements.txt
```

### How do I create a new virtual environment?

You only need the virtualenv tool itself when you want to create a new environment.
This is really simple.
Start by changing directory into the root of your project directory,
and then use the virtualenv command-line tool to create a new environment:

```
$ cd ~/code/myproject/
$ virtualenv env
```


Here, env is just the name of the directory you want to create your virtual environment inside. It's a common convention to call this directory env, and to put it inside your project directory (so, say you keep your code at ~/code/projectname/, the environment will be at ~/code/projectname/env/ - each project gets its own env).
But you can call it whatever you like and put it wherever you like!

Note: if you're using a version control system like git, you shouldn't commit the env directory. Add it to your .gitignore file (or similar).

### How do I use my shiny new virtual environment?

If you look inside the env directory you just created, you'll see a few subdirectories:

```
$ ls env
bin include lib
```

The one you care about the most is bin. This is where the local copy of the python binary and the pip installer exists. Let's start by using the copy of pip to install requests into the virtualenv (rather than globally):



$ env/bin/pip install requests
Downloading/unpacking requests
  Downloading requests-1.1.0.tar.gz (337kB): 337kB downloaded
  Running setup.py egg_info for package requests

Installing collected packages: requests
  Running setup.py install for requests

Successfully installed requests
Cleaning up...

It worked! Notice that we didn't need to use sudo this time,
because we're not installing requests globally,
we're just installing it inside our home directory.

Now, instead of typing python to get a Python shell, we type env/bin/python, and then...

```
>>> import requests
>>> requests.get('http://dabapps.com')
<Response [200]>
```

### But that's a lot of typing!

virtualenv has one more trick up its sleeve.
Instead of typing env/bin/python and env/bin/pip every time,
 we can run a script to activate the environment.
This script, which can be executed with source env/bin/activate,
simply adjusts a few variables in your shell (temporarily) so that when you type python,
you actually get the Python binary inside the virtualenv instead of the global one:


```
$ which python
/usr/bin/python
$ source env/bin/activate
$ which python
/Users/jamie/code/myproject/env/bin/python
```

So now we can just run pip install requests (instead of env/bin/pip install requests) and pip will install the library into the environment, instead of globally.
The adjustments to your shell only last for as long as the terminal is open, so you'll need to remember to rerun source env/bin/activate each time you close and open your terminal window.
If you switch to work on a different project (with its own environment) you can run deactivate to stop using one environment, and then source env/bin/activate to activate the other.

Activating and deactivating environments does save a little typing,
but it's a bit "magical" and can be confusing.
Make your own decision about whether you want to use it.


#### Requirements files

virtualenv and pip make great companions,
especially when you use the requirements feature of pip.
Each project you work on has its own requirements.txt file,
and you can use this to install the dependencies for that project into its virtual environment:

```
env/bin/pip install -r requirements.txt
```


### Data Structured

aList = []
aList = list()
len(aList)


#
